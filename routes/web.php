<?php

use App\Http\Controllers\CompanyC;
use App\Http\Controllers\DesaC;
use App\Http\Controllers\EmployeeC;
use App\Http\Controllers\KabupatenC;
use App\Http\Controllers\ProvinsiC;
use App\Http\Controllers\KecamatanC;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/employees', EmployeeC::class);
Route::get('/employees/export/', 'EmployeeC@export')->name('exportemployee');

Route::resource('/companies', CompanyC::class);


Route::resource('/provinsi', ProvinsiC::class);
Route::resource('/kabupaten', KabupatenC::class);
Route::resource('/kecamatan', KecamatanC::class);
Route::resource('/desa', DesaC::class);