<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table style="border: 1px solid black">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Posisi</th>
            <th>Perusahaan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($employee as $no => $item)
            <tr>
                <td>{{ $no }}</td>
                <td>{{$item->nama}}</td>
                <td>@if (is_null($item->atasan_id))
                    CEO
                @elseif ($item->atasan_id == 1)
                    Direktur
                @elseif ($item->atasan_id == 2 || $item->atasan_id == 3)
                    Manage
                @else
                    Staff
                @endif</td>
                <td>{{$item->company->nama}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
