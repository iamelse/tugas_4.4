@include('template.head')
    <div id="app">
        
        @include('template.sidebar')

        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>

            <div class="page-heading">
                <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Daftar Provinsi Indonesia</h3>
                            <p class="text-subtitle text-muted"></p>
                        </div>
                    </div>
                </div>

                <!-- Basic Tables start -->
                <section class="section">
                    <div class="row" id="basic-table">
                        <div class="col-12 col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">

                                        @if ($message = Session::get('success'))
                                            <div class="alert alert-success" role="alert">
                                                {{$message}}
                                            </div>
                                        @endif

                                        <!-- Table with outer spacing -->
                                        <div class="table-responsive">
                                            <table class="table table-lg">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama Kecamatan</th>
                                                        <th>Nama Kabupaten</th>
                                                        <th>Nama Provinsi</th>
                                                        <th>Jumlah Desa</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    @foreach ($kecamatan as $num => $item)
                                                        <td>{{ $num + 1 }}</td>
                                                        <td>{{ $item->nama }}</td> 
                                                        <td>{{ $item->kabupaten }}</td>
                                                        <td>{{ $item->provinsi->nama }}</td>
                                                        <td>{{ $item->desa->count() }}</td>
                                                        <td>

                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <span>
                                                                        <a href="{{ route('companies.edit', $item->id) }}" class="btn icon"><i class='bx bx-pencil fs-5 text-warning' ></i></a>
                                                                    </span>
                                                                </div>
                                                                <div class="col-2">
                                                                    <form action="{{ route('companies.destroy', $item->id) }}" method="POST">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <span>
                                                                            <button class="btn icon"><i class='bx bx-trash text-danger fs-5' ></i></button>
                                                                        </span>
                                                                    </form>
                                                                </div>
                                                            </div> 

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <a href="{{ route('provinsi.create') }}" class="btn btn-primary">Tambah Provinsi</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Tables end -->

            </div>

@include('template.footer')