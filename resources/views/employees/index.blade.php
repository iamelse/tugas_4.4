@include('template.head')
    <div id="app">
        
        @include('template.sidebar')

        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>

            <div class="page-heading">
                <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Daftar Pegawai</h3>
                            <p class="text-subtitle text-muted">Melihat Daftar Pegawai yang ada.</p>
                        </div>
                    </div>
                </div>

                <!-- Basic Tables start -->
                <section class="section">
                    <div class="row" id="basic-table">
                        <div class="col-12 col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">

                                        @if ($message = Session::get('success'))
                                            <div class="alert alert-success" role="alert">
                                                {{$message}}
                                            </div>
                                        @endif

                                        <!-- Table with outer spacing -->
                                        <div class="table-responsive">
                                            <table class="table table-lg">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Nama Pegawai</th>
                                                        <th>Posisi</th>
                                                        <th>Perusahaan</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($employee as $item)
                                                    <tr>
                                                        <td class="text-bold-500">{{ $item->id }}</td>
                                                        <td>{{ $item->nama }}</td>
                                                        <td>@if (is_null($item->atasan_id))
                                                            CEO
                                                        @elseif ($item->atasan_id == 1)
                                                            Direktur
                                                        @elseif ($item->atasan_id == 2 || $item->atasan_id == 3)
                                                            Manager
                                                        @else
                                                            Staff
                                                        @endif</td>
                                                        <td>{{ $item->company->nama }}</td>
                                                        <td>

                                                            <div class="row">
                                                                <div class="col-2">
                                                                    <span>
                                                                        <a href="{{ route('employees.edit', $item->id) }}" class="btn icon"><i class='bx bx-pencil fs-5 text-warning' ></i></a>
                                                                    </span>
                                                                </div>
                                                                <div class="col-2">
                                                                    <form action="{{ route('employees.destroy', $item->id) }}" method="POST">
                                                                        @csrf
                                                                        @method('delete')
                                                                        <span>
                                                                            <button class="btn icon"><i class='bx bx-trash text-danger fs-5' ></i></button>
                                                                        </span>
                                                                    </form>
                                                                </div>
                                                            </div> 

                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="btn">
                                                <a href="{{ route('employees.create') }}" class="btn btn-primary">Tambah Pegawai</a>
                                            </div>
    
                                            <div class="btn">
                                                <a href="{{ route('exportemployee') }}?type=pdf" class="btn btn-sm btn-info">.PDF</a>
                                                <a href="{{ route('exportemployee') }}?type=excel" class="btn btn-sm btn-info">.XLSX</a>
                                            </div> 
                                        </div>                           

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Tables end -->

            </div>
@include('template.footer')