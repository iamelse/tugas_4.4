@include('template.head')
    <div id="app">
        
        @include('template.sidebar')

        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>

            <div class="page-heading">
                <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last">
                            <h3>Tambah Kabupaten</h3>
                            <p class="text-subtitle text-muted">Ada</p>
                        </div>
                    </div>
                </div>

                <!-- // Basic multiple Column Form section start -->
                <section id="multiple-column-form">
                    <div class="row match-height">
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="{{ route('kabupaten.store') }}" class="form form-vertical" method="POST">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <h6>Nama Provinsi</h6>
                                                            <fieldset class="form-group">
                                                                <select name="provinsi_id" class="form-select" id="basicSelect">
                                                                    <option value="null">(Pilih Provinsi)</option>
                                                                    @foreach ($provinsi as $num => $item)
                                                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <h6>Nama Kabupaten</h6>
                                                            <input type="text" id="first-name-vertical"
                                                                class="form-control" name="nama"
                                                                placeholder="Semarang">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 d-flex justify-content-end">
                                                        <button type="submit"
                                                            class="btn btn-primary me-1 mb-1">Submit</button>
                                                        <button type="reset"
                                                            class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form> @dd($provinsi)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic multiple Column Form section end -->

            </div>

@include('template.footer')