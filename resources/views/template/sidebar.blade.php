<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="index.html"><img src="{{ asset('mazer/assets/images/logo/logo.png') }}" alt="Logo" srcset=""></a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-title">Menu</li>

                <li class="sidebar-item  ">
                    <a href="{{ route('employees.index') }}" class='sidebar-link'>
                        <i class='bx bxs-contact' ></i>
                        <span>Pegawai</span>
                    </a>
                </li>

                <li class="sidebar-item  ">
                    <a href="{{ route('companies.index') }}" class='sidebar-link'>
                        <i class='bx bx-building'></i>
                        <span>Perusahaan</span>
                    </a>
                </li>

                <li class="sidebar-item  ">
                    <a href="{{ route('provinsi.index') }}" class='sidebar-link'>
                        <i class='bx bx-building'></i>
                        <span>Provinsi</span>
                    </a>
                </li>

                <li class="sidebar-item  ">
                    <a href="{{ route('kabupaten.index') }}" class='sidebar-link'>
                        <i class='bx bx-building'></i>
                        <span>Kabupaten</span>
                    </a>
                </li>

                <li class="sidebar-item  ">
                    <a href="{{ route('kecamatan.index') }}" class='sidebar-link'>
                        <i class='bx bx-building'></i>
                        <span>Kecamatan</span>
                    </a>
                </li>

                <li class="sidebar-item  ">
                    <a href="{{ route('desa.index') }}" class='sidebar-link'>
                        <i class='bx bx-building'></i>
                        <span>Desa</span>
                    </a>
                </li>


            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>