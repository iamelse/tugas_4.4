<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Kabupaten;

class Provinsi extends Model
{
    protected $table = 'provinsis';
    protected $fillable = [
        'nama'
    ];

    public function kabupaten()
    {
        return $this->hasMany(Kabupaten::class, 'provinsi_id');
    }

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class);
    }

    public function desa()
    {
        return $this->hasMany(Desa::class);
    }
}
