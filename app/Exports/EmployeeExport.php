<?php

namespace App\Exports;

use App\Models\Employee;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class EmployeeExport implements FromCollection{

    public function collection()
    {
        return Employee::all();
    }

    public function view(): View
    {
        return view('exports.employee', [
            'employee' => Employee::all()
        ]);
    }

}
